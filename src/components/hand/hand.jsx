import React from 'react';
import Card from './styles/card';

export default class Hand extends React.Component {

    render() {
        let cards = [];
        let key = 0;

        this.props.cards.forEach(card => {
            cards.push(
                <Card key={key += 1} suit={card.suit} rank={card.rank} />
            );
        });

        return (
            <div className="hand">
                <h1>{this.props.name}</h1>
                <div>{cards}</div>
            </div>
        );
    }
}