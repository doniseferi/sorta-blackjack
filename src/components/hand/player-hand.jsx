import React from 'react';
import Hand from './hand';
import './styles/player-hand-styles.css';
import '../shadowBorder.css';

export default class PlayerHand extends React.Component {

    render() {
        return (
            <div className="handContainer shadowBorder" >
                <h1 key={0}>{this.props.name}</h1>
                <Hand key={1} cards={this.props.cards} />
            </div>);
    }
}