import React from 'react';
import './styles/card-styles.css';

export default class Card extends React.Component {

    constructor(props) {
        super(props);
        this.props = props;
    }

    render() {
        return (
            <div className={'card suit' + this.props.suit.toLowerCase()}>
                <p>{this.props.rank}</p>
            </div>
        );
    }
}