import React from 'react';
import PlayerSummary from './playersummary';
import './styles/game-summary-style.css';
import '../common-styles/shadowBorder.css';

export default class GameSummary extends React.Component {


    constructor() {
        super();
        this.state = {

            players: [
                {
                    id: 1,
                    score: 19,
                    name: 'Player 1',
                    hit: true,
                },
                {
                    id: 2,
                    score: 20,
                    name: 'Player 2',
                    hit: true,
                },
                {
                    id: 3,
                    score: 21,
                    name: "Dealer",
                    hit: false,
                }
            ]
        }
    }

    render() {
        return (
            <div className='summaryContainer shadowBorder'>
                {this.state.players.map(player => <PlayerSummary key={player.id} player={player} onClick={() => this.handleClick(player)} />)}
            </div>);
    }

    handleClick(player) {
        let players = [...this.state.players];
        let updatedPlayer = players.find((p) => { return p.id === player.id });
        let hitValue = updatedPlayer.hit;
        updatedPlayer.hit = !hitValue;
        this.setState({ players: players })
    }
}