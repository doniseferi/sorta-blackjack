import React from 'react';
import './styles/player-summary-style.css';

export default class PlayerSummary extends React.Component {

    render() {
        return (
            <div className="playerSummary">
                <a className='hitMeButton' onClick={() => this.props.onClick(this.props.player)}>{this.props.player.hit ? 'Hitting' : 'Standing'}</a>
                <span>{`${this.props.player.name} has ${this.props.player.score}`}</span>
            </div>);
    }
}