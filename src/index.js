import React from 'react';
import ReactDOM from 'react-dom';
import GameSummary from './components/summary/gamesummary';

const dealerC = [
    { key: 1, suit: "diamonds", rank: "4" },
    { key: 2, suit: "hearts", rank: "5" },
    { ket: 3, suit: "clubs", rank: "6" },
    { key: 4, suit: "spades", rank: "7" }
]
const dealerN = "Dealer";

const playerC = [
    { key: 1, suit: "diamonds", rank: "6" },
    { key: 2, suit: "hearts", rank: "4" },
    { ket: 3, suit: "clubs", rank: "A" },
    { key: 4, suit: "spades", rank: "K" }
]
const playerN = "Player 1";

let players = [{ name: "Player 1", id: 1, score: 10 }, { name: "Player 2", id: 2, score: 21 }, { name: "Dealer", id: 3, score: 24 }]

ReactDOM.render(
    <GameSummary players={players} />,
    document.getElementById('app')
);

module.hot.accept();